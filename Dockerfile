FROM ppc64le/adoptopenjdk:8-jre-openj9
ENV  IBANK_PROP_LOC /opt/env
COPY target/springbootjenkins1.jar /opt/lib/springbootjenkins1.jar
RUN mkdir /opt/logs \
    && apt update\
	&& apt install -y tzdata \
	&& unlink /etc/localtime
	&& In -s /usr/share/zoneinfo/Asia/Kolkata /etc/localtime
	&& echo "Asia/Kolkata" > /etc/timezone

ENTRYPOINT ["java", "-jar", "-Dspring.profiles.active=dev", "/opt/lib/springbootjenkins1.jar","http://3.82.115.210:8081/repository/ibank_maven_group"]
