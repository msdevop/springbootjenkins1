package com.msdevop.dockerex.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountsController {

	@GetMapping("/sayHello")

	public String sayHello() {

		return "Welcome! to First kubernetes ticket book application";
	}
	
	@GetMapping("/health")
	public String health() {

		return "OK";
	}

}
